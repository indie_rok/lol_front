(()=>{
  angular
    .module('riot',['ngResource','ngRoute']);


  var element = $('#backtotop'),
      originalY = 765;
  var topMargin = 765;
  $(window ).scroll(function() {
    var scrollv = $(window).scrollTop();
    $('#backtotop').stop(false,false).animate({
        top: scrollv < originalY
          ? 765
          : scrollv - originalY + topMargin
      }, 0);

    if(scrollv >388) {
      if (scrollv < $('body').height() - 380) {
        $('#backtotop2').stop(false, false).animate({
          top: scrollv < 100
            ? 100
            : scrollv - 288
        }, 0);
      }
    }
    //}
  });


})();

function menuInit() {
  $('#navlogo a').on('click', function(e){
    console.log('c');
    $('#submenu').addClass('hidden');
    $('#navbar a').removeClass('active');
    $('#noticias').addClass('active');
    $('header-groups').hide();
    $('upcoming').show();
    $('body').removeClass('bgp');
  });
  $('#navbar a').on('click', function (e) {
    $('#navbar a').removeClass('active');
    $(e.target).addClass('active');
    $('#submenu').addClass('hidden');
    $('header-groups').hide();
    $('upcoming').show();
    $('body').removeClass('bgp');
    if ($(e.target).attr('id') == 'lacopa') {
      $('#submenu').removeClass('hidden');
      $('#submenu ul').hide();
      $('#submenu').removeClass('other');
      $('#to_copa a').removeClass('active');
      $('#to_copa a').first().addClass('active');
      $('#to_copa').show();
    }
    if ($(e.target).attr('id') == 'otraligas') {
      $('#submenu').removeClass('hidden');
      $('#submenu ul').hide();
      $('#submenu').addClass('other');
      $('#to_otras a').removeClass('active');
      $('#to_otras a').first().addClass('active');
      $('#to_otras').show();
      $('body').addClass('bgp');
    }


    if ($(e.target).attr('id') == 'calendario') {
      $('header-groups').show();
      $('upcoming').hide();
    }
  });

  $('#submenu a').on('click', function (e) {
    $('#submenu a').removeClass('active');
    $(e.target).addClass('active');
  });
}