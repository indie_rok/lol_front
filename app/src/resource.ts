(()=>{
  angular
    .module('riot')
    .factory('api', api)
    .factory('production',production);

  api.$inject = ['$resource'];
  function api($resource){
    return $resource('/data/:type.json',{});
  }

  production.$inject = ['$resource'];
  function production($resource){
    return $resource('http://lol-grupos.herokuapp.com/api/:type/:filter',{});
    //return $resource('http://lol-pruebas.herokuapp.com/api/:type/:filter',{});
  }
})();