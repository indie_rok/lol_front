(()=>{
  angular
    .module('riot')
    .config(config);

  config.$inject = ['$routeProvider','$sceProvider'];

  var resolveHome = {
    options   : dataOptions,
    articles  : dataArticles,
    groups    : dataGroups,
    table     : dataTable
  };

  var resolveTournament = {
    teams     : dataTeams,
    texts     : dataTexts,
    options   : dataOptions
  };

  var resolveRules = {
    rules     : dataTexts
  };

  var resolveCalendar = {
    calendar  : dataCalendar,
    calendar_old  : dataCalendarOld,
    options   : dataOptions,
  };

  var resolveOtras = {
    texts : dataTexts
  }


  dataTable.$inject = ['production'];
  function dataTable(api){
    return api.query({'type':'leaderboard'});
  }

  dataOptions.$inject = ['production'];
  function dataOptions(api){
    return api.get({'type':'options'});
  }

  dataArticles.$inject = ['production'];
  function dataArticles(api){
    return api.query({'type':'articles'});
  }

  dataGroups.$inject = ['production'];
  function dataGroups(api){
    return api.query({'type':'teams','filter':'by_group'});
  }

  dataTexts.$inject = ['production'];
  function dataTexts(api){
    return api.get({'type':'texts'});
  }

  dataTeams.$inject = ['production'];
  function dataTeams(api){
    return api.query({'type':'teams'});
  }

  dataCalendar.$inject = ['production'];
  function dataCalendar(api){
    return api.query({'type':'calendar'});
  }

  dataCalendarOld.$inject = ['production'];
  function dataCalendarOld(api){
    return api.query({'type':'calendar','filter':'old'});
  }

  function config($routeProvider,$sceProvider){
    $sceProvider.enabled(false);

    $routeProvider
    .when('/', {
      template: '<home data="$resolve"></home>',
      resolve : resolveHome
    })
    .when('/el-mundial', {
      template: '<tournament data="$resolve"></tournament>',
      resolve : resolveTournament
    })
    .when('/el-mundial/reglamento-global', {
      template: '<global-rules data="$resolve"></global-rules>',
      resolve : resolveRules
    })
    .when('/el-mundial/reglamento-local', {
      template: '<local-rules data="$resolve"></local-rules>',
      resolve : resolveRules
    })
    .when('/calendario', {
      template: '<calendar data="$resolve"></calendar>',
      resolve: resolveCalendar
    })
    .when('/otras-ligas', {
        template: '<otras data="$resolve"></otras>',
        resolve: resolveOtras
    })

    .when('/otras-ligas/cdl', {
        template: '<otrascdl data="$resolve"></otrascdl>',
        resolve: resolveOtras
    })
    .otherwise('/');
  }
})();