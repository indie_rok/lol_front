(()=>{
  let localrules = {
    templateUrl:'./app/templates/components/localrules.html',
    bindings:{
      data: '='
    }
  };

  angular
    .module('riot')
    .component('localRules', localrules);


})();