(()=>{
  let home = {
    templateUrl:'./app/templates/components/home.html',
    bindings:{
      data: '='
    }
  };

  angular
    .module('riot')
    .component('home', home);
})();