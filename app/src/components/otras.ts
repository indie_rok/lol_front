(()=>{
  let otras = {
    templateUrl:'./app/templates/components/otras.html',
    bindings:{
      data: '='
    }
  };

  angular
    .module('riot')
    .component('otras', otras);
})();

(()=>{
  let otrascdl = {
    templateUrl:'./app/templates/components/otrascdl.html',
    bindings:{
      data: '='
    }
  };

  angular
    .module('riot')
    .component('otrascdl', otrascdl);
})();