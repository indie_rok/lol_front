(()=>{
  let submenu = {
    templateUrl:'./app/templates/components/submenu.html',
    controller: menuCtrl
  };

  angular
    .module('riot')
    .component('submenu', submenu);

  menuCtrl.$inject = ['production'];
  function menuCtrl(api){
    var ctrl = this;
    ctrl.menu = null;
    ctrl.$onInit = onInit;

    function onInit(){
      api.query({'type':'menu'}).$promise.then(
        function(data){
          ctrl.menu = data;
        }
      );
    }
  }
})();