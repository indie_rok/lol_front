(()=>{
  let calendar = {
    templateUrl:'./app/templates/components/calendar.html',
    bindings:{
      data: '='
    },
    controller: ctrlCalendar
  };

  angular
    .module('riot')
    .component('calendar', calendar);

  ctrlCalendar.$inject = ['$filter'];
  function ctrlCalendar($filter){
    var ctrl = this;
    ctrl.options = null;
    ctrl.data.options.$promise.then(
      function(response) {

        ctrl.options = response;

        if(typeof ctrl.options.current_journey != "undefined"){
          ctrl.options.current_journey = ctrl.options.current_journey-1;
        }

        if (!response.group_table) {
          var _calendar = [];
          ctrl.data.calendar = null;
          ctrl.data.calendar_old.$promise.then(
            function (d) {

              for (var i in d) {
                if (d[i].id) {
                  var obj = {
                    id: d[i].id,
                    name: d[i].name,
                    date: null,
                    groups: []
                  };

                  var _g = {};
                  for (var m in d[i].matches) {
                    var _tmp = d[i].matches[m];
                    var c = new Date(_tmp.time);

                    var _i = c.getFullYear() + "" + (c.getMonth() + 1) + "" + c.getDate();
                    if (!(d[i].name > 0)) {
                      _i = 'e';
                    }
                    if (typeof _g[_i] == 'undefined') {
                      _g[_i] = {'date': _tmp.time, matches: []};
                    }
                    _g[_i].matches.push(_tmp);

                  }

                  if(!d[i].matches.length && !(d[i].name > 0)){
                      var dummy = [
                        {
                          score_a: 0,
                          score_b: 0,
                          time: null,
                          teams: []
                        },
                        {
                          score_a: 0,
                          score_b: 0,
                          time: null,
                          teams: []
                        }
                      ];

                      _g['e'] = {'date': null , matches: dummy};
                  }

                  for (var x in _g) {
                    var _group = {
                      group: $filter('date')(_g[x].date, 'EEEE dd/MM'),
                      matches: _g[x].matches
                    };
                    obj.groups.push(_group);
                  }
                  _calendar.push(obj);
                }
              }
              console.log(_calendar);
              ctrl.data.calendar = _calendar;
            }
          );
        }


      }
    );

  }

})();