(()=>{
  let header = {
    templateUrl:'./app/templates/components/menu.html',
    controller: menuCtrl
  };

  angular
    .module('riot')
    .component('menuRiot', header);

  menuCtrl.$inject = ['production','$location'];
  function menuCtrl(api, $location){
    var ctrl = this;
    ctrl.menu = null;
    ctrl.options = null;
    ctrl.$onInit = onInit;



    function onInit(){
      ctrl.options = api.get({'type':'options'});
      api.query({'type':'menu'}).$promise.then(
        function(data){
          ctrl.menu = data;
          function c() {
            switch ($location.$$path) {
              case '/otras-ligas':
                $('#otraligas').click();
                break;
              case '/calendario':
                $('#calendario').click();
                break;
              case '/':
                $('#noticias').click();
                break;
              case '/la-copa':
                $('#lacopa').click();
                break;
            }
          }
          setTimeout( menuInit, 800);
          setTimeout( c, 800);
        }
      );
    }
  }
})();