(()=>{
  let upcoming = {
    templateUrl:'./app/templates/components/upcoming.html',
    controller: upcomingCtrl
  };

  angular
    .module('riot')
    .component('upcoming', upcoming);

  upcomingCtrl.$inject = ['production'];
  function upcomingCtrl(api){
    var ctrl = this;
    ctrl.data = null;
    ctrl.$onInit = onInit;
    ctrl.back = back;
    ctrl.next = next;
    function onInit(){
      ctrl.data = api.query({'type': 'matches', 'filter': 'upcoming'});
    }

    var step = 1140;
    var current = 0;
    $('.carousel').attr('style','left:-'+0+'px');
    function next(){
      var max = Math.round(ctrl.data.length/3);
      current-=step;
      console.log(step*(max-1)*-1);
      if(current < step*(max-1)*-1 ){
        current+=step;
        return false;
      }
      $('.carousel').attr('style','left:'+current+'px');
    }

    function back(){
      current+=step;
      if(current > -0){
        current-=step;
        console.log(current);
        return false;
      }
      $('.carousel').attr('style','left:'+current+'px');
    }

  }
})();
