(()=>{
  let tournament = {
    templateUrl:'./app/templates/components/tournament.html',
    bindings:{
      data: '='
    }
  };
  angular
    .module('riot')
    .component('tournament', tournament);

})();