(()=>{
  let header = {
    templateUrl:'./app/templates/components/header_groups.html',
    controller: headerCtrl
  };

angular
  .module('riot')
  .component('headerGroups', header);

  headerCtrl.$inject = ['production','$interval'];
  function headerCtrl(api,$interval){
    var ctrl = this;
    ctrl.data = null;
    ctrl.selection = null;
    ctrl.groups = null;
    ctrl.$onInit = onInit;

    function onInit(){
      api.get({'type':'options'}).$promise.then(
        function(response){
          ctrl.groups = response.group_table;
          if(response.group_table){
            api.query({'type':'teams','filter':'by_group'}).$promise.then(
              function(data){
                ctrl.data = data;
                ctrl.selection = ctrl.data[0];
              }
            );

            var current = 0;
            function change(){
              if( current+1 < ctrl.data.length){
                current++;
              }else{
                current = 0;
              }
              ctrl.selection = ctrl.data[current];
            }
            $interval(change,5000);
          }else{
            api.query({'type':'leaderboard'}).$promise.then(
              function(data){
                ctrl.data = data;
              }
            );
          }

        }
      );

    }


  }
})();
