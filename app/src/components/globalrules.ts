(()=>{
  let globalrules = {
    templateUrl:'./app/templates/components/globalrules.html',
    bindings:{
      data: '='
    }
  };

  angular
    .module('riot')
    .component('globalRules', globalrules);
})();