(function () {
    angular
        .module('riot', ['ngResource', 'ngRoute']);
    var element = $('#backtotop'), originalY = 765;
    var topMargin = 765;
    $(window).scroll(function () {
        var scrollv = $(window).scrollTop();
        $('#backtotop').stop(false, false).animate({
            top: scrollv < originalY
                ? 765
                : scrollv - originalY + topMargin
        }, 0);
        if (scrollv > 388) {
            if (scrollv < $('body').height() - 380) {
                $('#backtotop2').stop(false, false).animate({
                    top: scrollv < 100
                        ? 100
                        : scrollv - 288
                }, 0);
            }
        }
        //}
    });
})();
function menuInit() {
    $('#navlogo a').on('click', function (e) {
        console.log('c');
        $('#submenu').addClass('hidden');
        $('#navbar a').removeClass('active');
        $('#noticias').addClass('active');
        $('header-groups').hide();
        $('upcoming').show();
        $('body').removeClass('bgp');
    });
    $('#navbar a').on('click', function (e) {
        $('#navbar a').removeClass('active');
        $(e.target).addClass('active');
        $('#submenu').addClass('hidden');
        $('header-groups').hide();
        $('upcoming').show();
        $('body').removeClass('bgp');
        if ($(e.target).attr('id') == 'lacopa') {
            $('#submenu').removeClass('hidden');
            $('#submenu ul').hide();
            $('#submenu').removeClass('other');
            $('#to_copa a').removeClass('active');
            $('#to_copa a').first().addClass('active');
            $('#to_copa').show();
        }
        if ($(e.target).attr('id') == 'otraligas') {
            $('#submenu').removeClass('hidden');
            $('#submenu ul').hide();
            $('#submenu').addClass('other');
            $('#to_otras a').removeClass('active');
            $('#to_otras a').first().addClass('active');
            $('#to_otras').show();
            $('body').addClass('bgp');
        }
        if ($(e.target).attr('id') == 'calendario') {
            $('header-groups').show();
            $('upcoming').hide();
        }
    });
    $('#submenu a').on('click', function (e) {
        $('#submenu a').removeClass('active');
        $(e.target).addClass('active');
    });
}
(function () {
    angular
        .module('riot')
        .factory('api', api)
        .factory('production', production);
    api.$inject = ['$resource'];
    function api($resource) {
        return $resource('/data/:type.json', {});
    }
    production.$inject = ['$resource'];
    function production($resource) {
        return $resource('http://lol-grupos.herokuapp.com/api/:type/:filter', {});
        //return $resource('http://lol-pruebas.herokuapp.com/api/:type/:filter',{});
    }
})();
(function () {
    angular
        .module('riot')
        .config(config);
    config.$inject = ['$routeProvider', '$sceProvider'];
    var resolveHome = {
        options: dataOptions,
        articles: dataArticles,
        groups: dataGroups,
        table: dataTable
    };
    var resolveTournament = {
        teams: dataTeams,
        texts: dataTexts,
        options: dataOptions
    };
    var resolveRules = {
        rules: dataTexts
    };
    var resolveCalendar = {
        calendar: dataCalendar,
        calendar_old: dataCalendarOld,
        options: dataOptions,
    };
    var resolveOtras = {
        texts: dataTexts
    };
    dataTable.$inject = ['production'];
    function dataTable(api) {
        return api.query({ 'type': 'leaderboard' });
    }
    dataOptions.$inject = ['production'];
    function dataOptions(api) {
        return api.get({ 'type': 'options' });
    }
    dataArticles.$inject = ['production'];
    function dataArticles(api) {
        return api.query({ 'type': 'articles' });
    }
    dataGroups.$inject = ['production'];
    function dataGroups(api) {
        return api.query({ 'type': 'teams', 'filter': 'by_group' });
    }
    dataTexts.$inject = ['production'];
    function dataTexts(api) {
        return api.get({ 'type': 'texts' });
    }
    dataTeams.$inject = ['production'];
    function dataTeams(api) {
        return api.query({ 'type': 'teams' });
    }
    dataCalendar.$inject = ['production'];
    function dataCalendar(api) {
        return api.query({ 'type': 'calendar' });
    }
    dataCalendarOld.$inject = ['production'];
    function dataCalendarOld(api) {
        return api.query({ 'type': 'calendar', 'filter': 'old' });
    }
    function config($routeProvider, $sceProvider) {
        $sceProvider.enabled(false);
        $routeProvider
            .when('/', {
            template: '<home data="$resolve"></home>',
            resolve: resolveHome
        })
            .when('/el-mundial', {
            template: '<tournament data="$resolve"></tournament>',
            resolve: resolveTournament
        })
            .when('/el-mundial/reglamento-global', {
            template: '<global-rules data="$resolve"></global-rules>',
            resolve: resolveRules
        })
            .when('/el-mundial/reglamento-local', {
            template: '<local-rules data="$resolve"></local-rules>',
            resolve: resolveRules
        })
            .when('/calendario', {
            template: '<calendar data="$resolve"></calendar>',
            resolve: resolveCalendar
        })
            .when('/otras-ligas', {
            template: '<otras data="$resolve"></otras>',
            resolve: resolveOtras
        })
            .when('/otras-ligas/cdl', {
            template: '<otrascdl data="$resolve"></otrascdl>',
            resolve: resolveOtras
        })
            .otherwise('/');
    }
})();
(function () {
    var calendar = {
        templateUrl: './app/templates/components/calendar.html',
        bindings: {
            data: '='
        },
        controller: ctrlCalendar
    };
    angular
        .module('riot')
        .component('calendar', calendar);
    ctrlCalendar.$inject = ['$filter'];
    function ctrlCalendar($filter) {
        var ctrl = this;
        ctrl.options = null;
        ctrl.data.options.$promise.then(function (response) {
            ctrl.options = response;
            if (typeof ctrl.options.current_journey != "undefined") {
                ctrl.options.current_journey = ctrl.options.current_journey - 1;
            }
            if (!response.group_table) {
                var _calendar = [];
                ctrl.data.calendar = null;
                ctrl.data.calendar_old.$promise.then(function (d) {
                    for (var i in d) {
                        if (d[i].id) {
                            var obj = {
                                id: d[i].id,
                                name: d[i].name,
                                date: null,
                                groups: []
                            };
                            var _g = {};
                            for (var m in d[i].matches) {
                                var _tmp = d[i].matches[m];
                                var c = new Date(_tmp.time);
                                var _i = c.getFullYear() + "" + (c.getMonth() + 1) + "" + c.getDate();
                                if (!(d[i].name > 0)) {
                                    _i = 'e';
                                }
                                if (typeof _g[_i] == 'undefined') {
                                    _g[_i] = { 'date': _tmp.time, matches: [] };
                                }
                                _g[_i].matches.push(_tmp);
                            }
                            if (!d[i].matches.length && !(d[i].name > 0)) {
                                var dummy = [
                                    {
                                        score_a: 0,
                                        score_b: 0,
                                        time: null,
                                        teams: []
                                    },
                                    {
                                        score_a: 0,
                                        score_b: 0,
                                        time: null,
                                        teams: []
                                    }
                                ];
                                _g['e'] = { 'date': null, matches: dummy };
                            }
                            for (var x in _g) {
                                var _group = {
                                    group: $filter('date')(_g[x].date, 'EEEE dd/MM'),
                                    matches: _g[x].matches
                                };
                                obj.groups.push(_group);
                            }
                            _calendar.push(obj);
                        }
                    }
                    console.log(_calendar);
                    ctrl.data.calendar = _calendar;
                });
            }
        });
    }
})();
(function () {
    var globalrules = {
        templateUrl: './app/templates/components/globalrules.html',
        bindings: {
            data: '='
        }
    };
    angular
        .module('riot')
        .component('globalRules', globalrules);
})();
(function () {
    var header = {
        templateUrl: './app/templates/components/ind_groups.html',
        controller: headerCtrl
    };
    angular
        .module('riot')
        .component('headerGroups', header);
    headerCtrl.$inject = ['production', '$interval'];
    function headerCtrl(api, $interval) {
        var ctrl = this;
        ctrl.data = null;
        ctrl.selection = null;
        ctrl.groups = null;
        ctrl.$onInit = onInit;
        function onInit() {
            api.get({ 'type': 'options' }).$promise.then(function (response) {
                ctrl.groups = response.group_table;
                if (response.group_table) {
                    api.query({ 'type': 'teams', 'filter': 'by_group' }).$promise.then(function (data) {
                        ctrl.data = data;
                        ctrl.selection = ctrl.data[0];
                    });
                    var current = 0;
                    function change() {
                        if (current + 1 < ctrl.data.length) {
                            current++;
                        }
                        else {
                            current = 0;
                        }
                        ctrl.selection = ctrl.data[current];
                    }
                    $interval(change, 5000);
                }
                else {
                    api.query({ 'type': 'leaderboard' }).$promise.then(function (data) {
                        ctrl.data = data;
                    });
                }
            });
        }
    }
})();
(function () {
    var home = {
        templateUrl: './app/templates/components/home.html',
        bindings: {
            data: '='
        }
    };
    angular
        .module('riot')
        .component('home', home);
})();
(function () {
    var localrules = {
        templateUrl: './app/templates/components/localrules.html',
        bindings: {
            data: '='
        }
    };
    angular
        .module('riot')
        .component('localRules', localrules);
})();
(function () {
    var header = {
        templateUrl: './app/templates/components/menu.html',
        controller: menuCtrl
    };
    angular
        .module('riot')
        .component('menuRiot', header);
    menuCtrl.$inject = ['production', '$location'];
    function menuCtrl(api, $location) {
        var ctrl = this;
        ctrl.menu = null;
        ctrl.options = null;
        ctrl.$onInit = onInit;
        function onInit() {
            ctrl.options = api.get({ 'type': 'options' });
            api.query({ 'type': 'menu' }).$promise.then(function (data) {
                ctrl.menu = data;
                function c() {
                    switch ($location.$$path) {
                        case '/otras-ligas':
                            $('#otraligas').click();
                            break;
                        case '/calendario':
                            $('#calendario').click();
                            break;
                        case '/':
                            $('#noticias').click();
                            break;
                        case '/la-copa':
                            $('#lacopa').click();
                            break;
                    }
                }
                setTimeout(menuInit, 800);
                setTimeout(c, 800);
            });
        }
    }
})();

(function () {
    var footer = {
        templateUrl: './app/templates/components/footer.html',
        controller: menuCtrl
    };
    angular
        .module('riot')
        .component('ftr', footer);
    menuCtrl.$inject = ['production', '$location'];
    function menuCtrl(api, $location) {
        var ctrl = this;
        ctrl.menu = null;
        ctrl.options = null;
        ctrl.$onInit = onInit;
        function onInit() {
            ctrl.options = api.get({ 'type': 'options' });
            api.query({ 'type': 'menu' }).$promise.then(function (data) {
                ctrl.menu = data;
                function c() {
                    switch ($location.$$path) {
                        case '/otras-ligas':
                            $('#otraligas').click();
                            break;
                        case '/calendario':
                            $('#calendario').click();
                            break;
                        case '/':
                            $('#noticias').click();
                            break;
                        case '/la-copa':
                            $('#lacopa').click();
                            break;
                    }
                }
                setTimeout(menuInit, 800);
                setTimeout(c, 800);
            });
        }
    }
})();


(function () {
    var otras = {
        templateUrl: './app/templates/components/otras.html',
        bindings: {
            data: '='
        }
    };
    angular
        .module('riot')
        .component('otras', otras);
})();
(function () {
    var otrascdl = {
        templateUrl: './app/templates/components/otrascdl.html',
        bindings: {
            data: '='
        }
    };
    angular
        .module('riot')
        .component('otrascdl', otrascdl);
})();
(function () {
    var submenu = {
        templateUrl: './app/templates/components/submenu.html',
        controller: menuCtrl
    };
    angular
        .module('riot')
        .component('submenu', submenu);
    menuCtrl.$inject = ['production'];
    function menuCtrl(api) {
        var ctrl = this;
        ctrl.menu = null;
        ctrl.$onInit = onInit;
        function onInit() {
            api.query({ 'type': 'menu' }).$promise.then(function (data) {
                ctrl.menu = data;
            });
        }
    }
})();
(function () {
    var tournament = {
        templateUrl: './app/templates/components/tournament.html',
        bindings: {
            data: '='
        }
    };
    angular
        .module('riot')
        .component('tournament', tournament);
})();
(function () {
    var upcoming = {
        templateUrl: './app/templates/components/upcoming.html',
        controller: upcomingCtrl
    };
    angular
        .module('riot')
        .component('upcoming', upcoming);
    upcomingCtrl.$inject = ['production'];
    function upcomingCtrl(api) {
        var ctrl = this;
        ctrl.data = null;
        ctrl.$onInit = onInit;
        ctrl.back = back;
        ctrl.next = next;
        function onInit() {
            ctrl.data = api.query({ 'type': 'matches', 'filter': 'upcoming' });
        }
        var step = 1140;
        var current = 0;
        $('.carousel').attr('style', 'left:-' + 0 + 'px');
        function next() {
            var max = Math.round(ctrl.data.length / 3);
            current -= step;
            console.log(step * (max - 1) * -1);
            if (current < step * (max - 1) * -1) {
                current += step;
                return false;
            }
            $('.carousel').attr('style', 'left:' + current + 'px');
        }
        function back() {
            current += step;
            if (current > -0) {
                current -= step;
                console.log(current);
                return false;
            }
            $('.carousel').attr('style', 'left:' + current + 'px');
        }
    }
})();
